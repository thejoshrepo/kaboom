package com.example.josh.kaboom;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static Button kaBoom;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kaBoom = (Button) findViewById(R.id.kaBoom);

        kaBoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "kaBoom!", Toast.LENGTH_SHORT).show();
                String destinationPhone = ((EditText) findViewById(R.id.numberInput)).getText().toString();
                String messageToSend = ((EditText) findViewById(R.id.messageInput)).getText().toString();
                String repeated = ((EditText) findViewById(R.id.repeatedInput)).getText().toString();
                Log.e("[JOSH DEBUG]", destinationPhone);
                Log.e("[JOSH DEBUG]", messageToSend);
                Log.e("[JOSH DEBUG]", repeated);
                sendTexts(destinationPhone, messageToSend, Integer.parseInt(repeated.trim()));
            }
        });
    }

    private void sendTexts(String destinationPhone, String messageToSend, int repeated){
        SmsManager smsManager = SmsManager.getDefault();
        for(int i = 0; i < repeated; i++) {
            smsManager.sendTextMessage(destinationPhone, null, messageToSend, null, null);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
